# A funçao de agendamento foi quebrada.
# Para ativar siga as instruçoes nas linhas 51, 69, 78

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import openpyxl as excel
import datetime
import time

print("teste")

print("\n-->Bem vindo ao Primeiro Whatsapp Bot da Brasal! <--\n")
print("1 - Apenas enviar pesquisa de satisfação\n")
print("2 - Apenas coletar respostas\n")
menu = input()
menu = int(menu)

if menu == 1:

    #função para ler contatos a partir de um arquivo de texto
    def readContacts(fileName):
        lst = []
        file = excel.load_workbook(fileName)
        sheet = file.active
        firstCol = sheet['A']
        for cell in range(len(firstCol)):
            contact = str(firstCol[cell].value)
            if contact == "None":
                break
            contact = "\"" + contact + "\""
            lst.append(contact)
        return lst

    # Target Contacts
    # Not tested on Broadcast
    targets = readContacts("contacts.xlsx")


    # can comment out below line
    print(targets)

    # Driver to open a browser
    chrome_opt = webdriver.ChromeOptions()

    # GPU desabilitada pq o pc da brasal é ruim
    chrome_opt.add_argument('--disable-gpu')

    # Store your session details
    chrome_opt.add_argument("--user-data-dir=user-data")
    driver = webdriver.Chrome(executable_path="chromedriver.exe", options=chrome_opt)

    #link to open a site
    driver.get("https://web.whatsapp.com/")

    # 10 sec wait time to load, if good internet connection is not good then increase the time
    # units in seconds
    # note this time is being used below also
    wait = WebDriverWait(driver, 10)
    wait5 = WebDriverWait(driver, 5)
    input("Scan the QR code and then press Enter")


    # Message to send list
    # 1st Parameter: Hours in 0-23               [QUEBRADO]
    # 2nd Parameter: Minutes                     [QUEBRADO]
    # 3rd Parameter: Seconds (Keep it Zero)      [QUEBRADO]
    # 4th Parameter: Message to send at a particular time
    # Put '\n' at the end of the message, it is identified as Enter Key
    # Else uncomment Keys.Enter in the last step if you dont want to use '\n'
    # Keep a nice gap between successive messages
    # Use Keys.SHIFT + Keys.ENTER to give a new line effect in your Message
    msgToSend = [
                    [0, 0, 0, "Pesquisa de satisfação. Por favor responda:\n"+
                    "*/fraco* se achou fraco\n"+
                    "*/normal* se achou normal\n"+
                    "*/top* se achou top\n"]
                ]


    # Count variable to identify the number of messages to be sent 
    # MANTER EM ZERO SENÃO DUPLICA CADA MENSAGEM
    count = 0
    while count<len(msgToSend):

        # Identify time [QUEBRADO]
        # Caso queira habilitar o agendamento descomente o proximo bloco de código.
        """
        curTime = datetime.datetime.now()
        curHour = curTime.time().hour
        curMin = curTime.time().minute
        curSec = curTime.time().second
        """

        # Esse bloco de código só existe para nao precisar apagar o proximo if.
        # Para habilitar o agendamento pode apagar essas proximas 3 linhas.
        curHour = msgToSend[count][0]
        curMin = msgToSend[count][1]
        curSec = msgToSend[count][2]

        # if time matches then move further
        if msgToSend[count][0]==curHour and msgToSend[count][1]==curMin and msgToSend[count][2]==curSec:
            # utility variables to tract count of success and fails
            success = 0
            sNo = 1
            failList = []

            # Iterate over selected contacts
            for target in targets:
                print(sNo, ". Target is: " + target)
                sNo+=1
                try:
                    # Select the target
                    x_arg = '//span[contains(@title,' + target + ')]'

                    try:
                        wait5.until(EC.presence_of_element_located((
                            By.XPATH, x_arg
                        )))
                    except:
                        # If contact not found, then search for it
                        inputSearchBox = driver.find_element_by_xpath('//*[@id="side"]/div[1]/div/label/div/div[2]')
                        time.sleep(0.5)

                        # click the search button
                        time.sleep(1)
                        inputSearchBox.clear()
                        inputSearchBox.send_keys(target[1:len(target) - 1])
                        print("Target Searched")
                        inputSearchBox.send_keys(Keys.ENTER)
                        # Increase the time if searching a contact is taking a long time
                        time.sleep(5)

                    # Select the target
                    driver.find_element_by_xpath(x_arg).click()
                    print("Target Successfully Selected")
                    time.sleep(2)

                    # Select the Input Box
                    inp_xpath = '//*[@id="main"]/footer/div[1]/div[2]/div/div[2]'
                    input_box = wait.until(EC.presence_of_element_located((
                        By.XPATH, inp_xpath)))
                    time.sleep(1)


                    # Send message
                    # target is your target Name and msgToSend is you message
                    input_box.send_keys("Olá, " + target[1:len(target) - 1] + "." + Keys.ENTER + msgToSend[count][3] + Keys.ENTER) # + Keys.ENTER (Uncomment it if your msg doesnt contain '\n')
                
                    # Link Preview Time, Reduce this time, if internet connection is Good
                    time.sleep(10)
                    input_box.send_keys(Keys.ENTER)
                    print("Successfully Send Message to : "+ target + '\n')
                    success+=1
                    time.sleep(0.5)

                except:
                    # If target Not found Add it to the failed List
                    print("Cannot find Target: " + target)
                    failList.append(target)
                    pass

            print("\nSuccessfully Sent to: ", success)
            print("Failed to Sent to: ", len(failList))
            print(failList)
            print('\n')
            count+=1
    
    driver.quit()

elif menu == 2:

    #função para ler contatos a partir de um arquivo de texto
    def readContacts(fileName):
        lst = []
        file = excel.load_workbook(fileName)
        sheet = file.active
        firstCol = sheet['A']
        for cell in range(len(firstCol)):
            contact = str(firstCol[cell].value)
            if contact == "None":
                break
            contact = "\"" + contact + "\""
            lst.append(contact)
        return lst

    # Target Contacts
    # Not tested on Broadcast
    targets = readContacts("contacts.xlsx")

    # can comment out below line
    print(targets)

    # Driver to open a browser
    chrome_opt = webdriver.ChromeOptions()

    # GPU desabilitada pq o pc da brasal é ruim
    chrome_opt.add_argument('--disable-gpu')

    # Store your session details
    chrome_opt.add_argument("--user-data-dir=user-data")
    driver = webdriver.Chrome(executable_path="chromedriver.exe", options=chrome_opt)

    #link to open a site
    driver.get("https://web.whatsapp.com/")

    # 10 sec wait time to load, if good internet connection is not good then increase the time
    # units in seconds
    # note this time is being used below also
    wait = WebDriverWait(driver, 10)
    wait5 = WebDriverWait(driver, 5)
    input("Scan the QR code and then press Enter")


    # utility variables to tract count of success and fails
    success = 0
    sNo = 1
    failList = []

    # Iterate over selected contacts
    for target in targets:
        print(sNo, ". Target is: " + target)
        sNo+=1
        try:
            # Select the target
            x_arg = '//span[contains(@title,' + target + ')]'
            try:
                wait5.until(EC.presence_of_element_located((
                    By.XPATH, x_arg
                )))
            except:
                # If contact not found, then search for it
                inputSearchBox = driver.find_element_by_xpath('//*[@id="side"]/div[1]/div/label/div/div[2]')
                time.sleep(0.5)

                # click the search button
                time.sleep(1)
                inputSearchBox.clear()
                inputSearchBox.send_keys(target[1:len(target) - 1])
                print("Target Searched")
                inputSearchBox.send_keys(Keys.ENTER)
                # Increase the time if searching a contact is taking a long time
                time.sleep(5)

            # Select the target
            driver.find_element_by_xpath(x_arg).click()
            print("Target Successfully Selected")
            time.sleep(2)

            # Le a ultima mensagem recebida e armazena em 'texto'
            post = driver.find_elements_by_class_name("message-in")
            ultimo = len(post) - 1
            penultimo = len(post) - 2
            texto_ultimo = post[ultimo].find_element_by_css_selector("span.selectable-text").text
            texto_penultimo = post[penultimo].find_element_by_css_selector("span.selectable-text").text
            texto_ultimo = texto_ultimo.lower()
            texto_penultimo = texto_penultimo.lower()
            

            # Checar se a mensagem recebida é valida
            try:

                if "fraco" in texto_penultimo:
                    resultado = "fraco"
                elif "normal" in texto_penultimo:
                    resultado = "normal"
                elif "top" in texto_penultimo:
                    resultado = "top"


                if "fraco" in texto_ultimo:
                    resultado = "fraco"
                elif "normal" in texto_ultimo:
                    resultado = "normal"
                elif "top" in texto_ultimo:
                    resultado = "top"

                # Select the Input Box
                inp_xpath = '//*[@id="main"]/footer/div[1]/div[2]/div/div[2]'
                input_box = wait.until(EC.presence_of_element_located((
                    By.XPATH, inp_xpath)))
                time.sleep(1)

                # Send message
                # target is your target Name and msgToSend is you message
                input_box.send_keys("Obrigado, " + target[1:len(target) - 1] + ". Vc achou "+ resultado + Keys.ENTER) # + Keys.ENTER (Uncomment it if your msg doesnt contain '\n')
                
                # Link Preview Time, Reduce this time, if internet connection is Good
                time.sleep(10)
                input_box.send_keys(Keys.ENTER)
                print("Successfully Send Message to : "+ target + '\n')
                success+=1
                time.sleep(0.5)
                pass
            except:
                errorMsg = '''*/fraco* se achou fraco
                    */normal* se achou normal
                    */top* se achou top'''

                # Send message
                # target is your target Name and msgToSend is you message
                input_box.send_keys("Não consegui entender, " + target[1:len(target) - 1] + Keys.ENTER + errorMsg + Keys.ENTER) # + Keys.ENTER (Uncomment it if your msg doesnt contain '\n')
                
                # Link Preview Time, Reduce this time, if internet connection is Good
                time.sleep(10)
                input_box.send_keys(Keys.ENTER)
                print("Cannot find Target: " + target)
                time.sleep(0.5)
                failList.append(target)
                pass
                
        except:
            # If target Not found Add it to the failed List
            print("Cannot find Target: " + target)
            failList.append(target)
            pass

    print("\nSuccessfully Sent to: ", success)
    print("Failed to Sent to: ", len(failList))
    print(failList)
    print('\n\n')
    driver.quit()